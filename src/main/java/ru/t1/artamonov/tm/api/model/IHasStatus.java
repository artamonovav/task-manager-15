package ru.t1.artamonov.tm.api.model;

import ru.t1.artamonov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}

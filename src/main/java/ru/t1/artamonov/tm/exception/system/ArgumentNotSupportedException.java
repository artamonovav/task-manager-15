package ru.t1.artamonov.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
